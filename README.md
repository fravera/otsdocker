# OTSDocker

## Currently Built Images (in the Container Repository of this git repo)
Final image to run BurninBox: `gitlab-registry.cern.ch/otsdaq/otsdocker/v2_05_05:BurninBox`

Based on: 
1. os+packages: `gitlab-registry.cern.ch/otsdaq/otsdocker/otsdaq:base`
2. with basic otsdaq installed: `gitlab-registry.cern.ch/otsdaq/otsdocker/v2_05_05:otsdaq_installed`
3. with cms_outertracker installed (including Ph2_ACF): `gitlab-registry.cern.ch/otsdaq/otsdocker/v2_05_05:outertracker`

## For Users

#### Running a Container
First, start docker then pull the image to your local machine. First, log in to the CERN container registry by running `docker login gitlab-registry.cern.ch`. Then pull the image by running `docker pull gitlab-registry.cern.ch/otsdaq/otsdocker/[IMAGE NAME]`.

Run the image via`docker run -it --privileged --hostname $HOSTNAME --network host -v $(pwd)/output_data:/data/CMSBurninBox [IMAGE NAME]`. 
* The `-v` option (volume mount) maps a local folder (and its contents) to a folder inside of the docker repo. Here, we mount the otsdaq output in the container (`/data/CMSBurninBox`) into a local folder (`$(pwd)/output_data`). You can add as many of these as you would like. Note, however, that the local folder will locally overwrite whatever is in the container, so you can't use this method to add local files to non-empty folders in the container.
* The `--hostname` gives the docker container the same hostname as your local machine (not its default behavior, but required for otsdaq), and `--network` allows otsdaq to use your local ports. 
* The `-it` option runs in "interactive mode" and gives you a terminal, and `--privileged` gives the container all root capabilities of the host machine.
* If you want to give this session a non-silly name, add the option `--name [NAME]` that can help you find the instance of the container later. 

Once you are inside the container, you can start to run otsdaq:
1. `source OTSDAQSetup.sh`
2. `ots -w`
    * This runs the otsdaq wizard mode, which fills into the configuration files all the local information about your machine. It should only need to be run once. 
3. `ots`
    * This runs otsdaq. All future controls and configurations of otsdaq are done from the website that is displayed on the terminal.
4. `MainController` to start the Burnin Box

#### Some docker tips and tricks
* To see what containers are running currently, run `docker ps`. You can see all stopped containers as well by running `docker ps -a`. 
* You can close the interactive terminal without stopping the session by running `ctrl+P ctrl+Q` (aka return to your host commandline, outside of the image, but the image is still running and will show up in `docker ps`). 
* You can restart a session that you did kill (for example, by typing `exit` in your interactive session), by running `docker start [SESSION NAME]` then `docker attach [SESSION NAME]`. This will resume the container with all of your previous actions and options you initally ran with.
* You can see which containers you've downloaded locally by typing `docker images`
* If docker is running and you are getting a `permission denied` error when you try to run any command, you might have to add your username to the docker group. See more information on how to do that [here](https://docs.docker.com/engine/install/linux-postinstall/). 

#### Other notes
If you are a developer on either the BurninBox or cms_outertracker projects and would like to push any changes you made during your docker session, you'll need to change the `origin` branch in the appropriate repo. This is not required if you'd just like to pull new changes.
1. `git remote rm origin`
2. `git remote add [url]` where here the URL should be the `ssh` one, not the `http` as is default in the image
3. `kinit [username]@FNAL.GOV`
4. `git pull origin develop`, then you should be able to push whatever you'd like.

## For Developers

This repo contains Dockerfiles to create 4 docker images
1. ots_base -- this installs libraries on top of centos7
2. ots_installed -- this installs and compiles OTSDAQ (currently v2_05_05)
3. ots_outertracker -- this installs the otsdaq_cmsoutertracker srcs, including Ph2_ACF. Run `source BurninBoxSetup.sh` upon starting the container. To build this container, one must use the fermilab VPN and kinit username@FNAL.GOV
4. ots_burninbox -- this installs the otsdaq_cmsburninbox srcs to run the BurninBox OTSDAQ configuration

Images can be built in the given directory by running 
`docker build -f Dockerfile otsdaq:[IMAGE-NAME] .`

Images are pushed to the container registry associated with this repository. See details for how to do that [here](https://linux.web.cern.ch/centos7/docs/docker/)

Links to repos pulled by this image -- each should have a tag corresponding to the otsdaq tag (i.e. tag `v2_05_05` in each repo were used to build the full image based on otsdaq tag `v2_05_05).
* [cms_outertracker](https://cdcvs.fnal.gov/redmine/projects/cmsoutertracker/repository)
* [cms_burninbox](https://cdcvs.fnal.gov/redmine/projects/cmsburninbox/repository)
* [Ph2_ACF middleware](https://gitlab.cern.ch/otsdaq/Ph2_ACF)




